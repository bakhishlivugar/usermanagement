package com.bakhishli.usermanagement.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.modelmapper.ModelMapper;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
public class Config {

    @Bean
    public ModelMapper getModelMapper(){
        return new ModelMapper();
    }
}
