package com.bakhishli.usermanagement.exception;

public class UserPasswordDoNotMatchException extends RuntimeException{
    private static final String MESSAGE = "Password do not match";

    public UserPasswordDoNotMatchException(){
        super(MESSAGE);
    }
}
