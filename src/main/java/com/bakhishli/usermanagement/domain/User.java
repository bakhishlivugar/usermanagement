package com.bakhishli.usermanagement.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = User.TABLE_NAME)
public class User {
    public static final String TABLE_NAME = "users";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Email
    @NotBlank(message = "Email is required a field")
    private String email;

    @NotBlank(message = "Password should not be left blank")
    @NotNull(message="Password is a required field")
    @Size(min=8, max=30, message= "Password must be equal to or greater than 8 characters and less than 30 characters")
    private String password;

    @NotBlank(message = "Password should not be left blank")
    @NotNull(message="Password is a required field")
    @Size(min=8, max=30, message= "Password must be equal to or greater than 8 characters and less than 30 characters")
    @Column(name = "password_confirmation")
    private String passwordConf;

    private Boolean privacyPolicyAccepted;
}
