package com.bakhishli.usermanagement.web.rest;

import com.bakhishli.usermanagement.service.dto.req.SignUpReqDto;
import com.bakhishli.usermanagement.service.dto.res.UserResDto;
import com.bakhishli.usermanagement.service.impl.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/v1/auth")
@RequiredArgsConstructor
public class UserController {

    private final UserServiceImpl userService;

    @PostMapping("/sign-up")
    public ResponseEntity<UserResDto> signUp(@RequestBody @Valid SignUpReqDto signUpReqDto){
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.signUp(signUpReqDto));
    }
}
