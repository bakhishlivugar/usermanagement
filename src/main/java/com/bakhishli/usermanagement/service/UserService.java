package com.bakhishli.usermanagement.service;

import com.bakhishli.usermanagement.service.dto.req.SignUpReqDto;
import com.bakhishli.usermanagement.service.dto.req.UserReqDto;
import com.bakhishli.usermanagement.service.dto.res.UserResDto;

public interface UserService {
//    UserResDto createUser(UserReqDto userReqDto);
//    UserResDto getById(Long id);
//    UserResDto update(Long id, UserReqDto userReqDto);
//    void delete(Long id);
    UserResDto signUp(SignUpReqDto signUpReqDto);
}
