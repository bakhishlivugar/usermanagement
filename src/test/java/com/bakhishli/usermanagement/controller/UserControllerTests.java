package com.bakhishli.usermanagement.controller;

import com.bakhishli.usermanagement.service.dto.req.SignUpReqDto;
import com.bakhishli.usermanagement.service.dto.res.UserResDto;
import com.bakhishli.usermanagement.service.impl.UserServiceImpl;
import com.bakhishli.usermanagement.web.rest.UserController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
public class UserControllerTests {

    private static final String BASE_URL = "/v1/auth/sign-up";

    @MockBean
    private UserServiceImpl userService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void whenValidDtoThenOk() throws Exception {
        SignUpReqDto signUpReqDto = new SignUpReqDto();
        signUpReqDto.setEmail("bakhishlivugar@gmail.com");
        signUpReqDto.setPassword("123456789");
        signUpReqDto.setPasswordConf("123456789");
        signUpReqDto.setPrivacyPolicyAccepted(true);

        mockMvc.perform(
                post("/v1/auth/sign-up")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(signUpReqDto)))
                .andExpect(status().isCreated());

        ArgumentCaptor<SignUpReqDto> captor = ArgumentCaptor.forClass(SignUpReqDto.class);
        verify(userService, times(1)).signUp(captor.capture());
        assertThat(captor.getValue().getEmail()).isEqualTo("bakhishlivugar@gmail.com");
        assertThat(captor.getValue().getPassword()).isEqualTo("123456789");
        assertThat(captor.getValue().getPasswordConf()).isEqualTo("123456789");
    }


    @Test
    void whenInvalidDtoThenBadRequest() throws Exception {
        SignUpReqDto signUpReqDto = new SignUpReqDto();
        signUpReqDto.setEmail(null);
        signUpReqDto.setPassword("123456789");
        signUpReqDto.setPasswordConf("123456789");
        signUpReqDto.setPrivacyPolicyAccepted(true);

        userService.signUp(signUpReqDto);

        mockMvc.perform(
                        post(BASE_URL)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(signUpReqDto)))
                .andExpect(status().isBadRequest());
    }
}
