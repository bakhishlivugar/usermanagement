package com.bakhishli.usermanagement.service.dto.req;

import lombok.Data;

@Data
public class UserReqDto {
    private String email;
    private String password;
    private String passwordConf;
    private Boolean privacyPolicyAccepted;
}
