package com.bakhishli.usermanagement.service.impl;

import com.bakhishli.usermanagement.exception.UserPasswordDoNotMatchException;
import com.bakhishli.usermanagement.repository.UserRepository;
import com.bakhishli.usermanagement.service.dto.req.SignUpReqDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @Spy
    private ModelMapper modelMapper;

    private  UserServiceImpl userService;


    @BeforeEach()
    void setUp(){
        userService = new UserServiceImpl(userRepository, modelMapper);
    }

    @Test
    void whenUserPasswordDoNotMatchThenThrowException() {
        SignUpReqDto signUpReqDto = new SignUpReqDto();
        signUpReqDto.setPassword("123456");
        signUpReqDto.setPasswordConf("42321");
        assertThatThrownBy(()-> userService.signUp(signUpReqDto))
                .isInstanceOf(UserPasswordDoNotMatchException.class);
    }
}