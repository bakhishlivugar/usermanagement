package com.bakhishli.usermanagement.service.dto.req;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class SignUpReqDto {

    @Email
    @NotNull
    private String email;

    @NotNull
    @NotBlank
    @Size(min = 8, max = 30)
    private String password;

    @NotNull
    @NotBlank
    private String passwordConf;

    @NotNull
    private Boolean privacyPolicyAccepted;
}
