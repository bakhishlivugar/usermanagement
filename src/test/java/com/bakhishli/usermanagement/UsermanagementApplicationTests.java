package com.bakhishli.usermanagement;

import com.bakhishli.usermanagement.domain.User;
import com.bakhishli.usermanagement.exception.EmailAlreadyExistsException;
import com.bakhishli.usermanagement.exception.UserPasswordDoNotMatchException;
import com.bakhishli.usermanagement.repository.UserRepository;
import com.bakhishli.usermanagement.service.dto.req.SignUpReqDto;
import com.bakhishli.usermanagement.service.dto.res.UserResDto;
import com.bakhishli.usermanagement.service.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Spy;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@SpringBootTest
class UsermanagementApplicationTests {

	public UserServiceImpl userService;

	@Mock
	private UserRepository userRepository;

	@Spy
	private ModelMapper modelMapper;

	@BeforeEach
	public final void before() {
		userService = new UserServiceImpl(userRepository,modelMapper);
	}

	@Test
	void whenGetEmailThenThrowEmailAlreadyExistException() {
		//Arrange
		User user = new User();
		user.setId(1L);
		user.setPassword("123456789");
		user.setPasswordConf("123456789");
		user.setEmail("bakhishlivugar@gmail.com");

		//Act
		when(userRepository.findByEmail(user.getEmail())).thenReturn(java.util.Optional.of(user));
		SignUpReqDto map = modelMapper.map(user, SignUpReqDto.class);

		//Assert
		assertThatThrownBy(()-> userService.signUp(map))
				.isInstanceOf(EmailAlreadyExistsException.class);
	}

	@Test
	void whenGetPasswordThenThrowPasswordDoesNotMatchException() {
		//Arrange
		User user = new User();
		user.setId(1L);
		user.setPassword("123456789");
		user.setPasswordConf("123456065");
		user.setEmail("bakhishlivugar@gmail.com");

		//Act
		SignUpReqDto map = modelMapper.map(user, SignUpReqDto.class);

		//Assert
		assertThatThrownBy(()-> userService.signUp(map))
				.isInstanceOf(UserPasswordDoNotMatchException.class);
	}

	@Test
	void whenGetPasswordThenThrowPasswordSizeDoesNotMatchException() {
		//Arrange
		User user = new User();
		user.setId(1L);
		user.setPassword("123456789123456789123456789123456789123456789123456789");
		user.setPasswordConf("123456789123456789123456789123456789123456789123456789");
		user.setEmail("bakhishlivugar@gmail.com");
		user.setPrivacyPolicyAccepted(true);

		//Act
		SignUpReqDto map = modelMapper.map(user, SignUpReqDto.class);

		//Assert
		assertThatThrownBy(()-> userService.signUp(map));
	}

	@Test
	void whenSaveUserThenGetDto() {
		//Arrange
		SignUpReqDto signUpReqDto = new SignUpReqDto();
		signUpReqDto.setEmail("bakhishlivugar@gmail.com");
		signUpReqDto.setPassword("123456789");
		signUpReqDto.setPasswordConf("123456789");
		signUpReqDto.setPrivacyPolicyAccepted(true);

		//Act
		User user = modelMapper.map(signUpReqDto, User.class);
		user.setId(1L);
		when(userRepository.save(any(User.class))).thenReturn(user);

		UserResDto userResDto = userService.signUp(signUpReqDto);

		//Assert
		assertThat(userResDto.getId()).isEqualTo(1L);
	}

}
