package com.bakhishli.usermanagement.service.impl;

import com.bakhishli.usermanagement.domain.User;
import com.bakhishli.usermanagement.exception.EmailAlreadyExistsException;
import com.bakhishli.usermanagement.exception.UserPasswordDoNotMatchException;
import com.bakhishli.usermanagement.repository.UserRepository;
import com.bakhishli.usermanagement.service.UserService;
import com.bakhishli.usermanagement.service.dto.req.SignUpReqDto;
import com.bakhishli.usermanagement.service.dto.res.UserResDto;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

//    @Override
//    public UserResDto createUser(UserReqDto userReqDto) {
//        User save = userRepository.save(modelMapper.map(userReqDto, User.class));
//        return modelMapper.map(save, UserResDto.class);
//    }
//
//    @Override
//    public UserResDto getById(Long id) {
//        User user = userRepository.findById(id)
//                .orElseThrow(() -> new RuntimeException("User not found with given id"));
//        return modelMapper.map(user, UserResDto.class);
//    }
//
//    @Override
//    public UserResDto update(Long id, UserReqDto userReqDto) {
//        return null;
//    }
//
//    @Override
//    public void delete(Long id) {
//        User user = userRepository.findById(id)
//                .orElseThrow(() -> new RuntimeException("User already deleted"));
//        userRepository.delete(user);
//    }

    @Override
    public UserResDto signUp(SignUpReqDto signUpReqDto) {
        Optional<User> email = userRepository.findByEmail(signUpReqDto.getEmail());
        if(email.isPresent()){
            throw new EmailAlreadyExistsException();
        }
        if(!signUpReqDto.getPassword().equals(signUpReqDto.getPasswordConf())){
            throw new UserPasswordDoNotMatchException();
        }

        User save = userRepository.save(modelMapper.map(signUpReqDto, User.class));
        return modelMapper.map(save, UserResDto.class);
    }


}
